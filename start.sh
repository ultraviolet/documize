#!/bin/bash
set -e

export DOCUMIZEPORT=5001
export DOCUMIZEDB="host=${CLOUDRON_POSTGRESQL_HOST} port=${CLOUDRON_POSTGRESQL_HOST} dbname=${CLOUDRON_POSTGRESQL_DATABASE} user=${CLOUDRON_POSTGRESQL_USERNAME} password=${CLOUDRON_POSTGRESQL_PASSWORD} sslmode=disable"
export DOCUMIZEDBTYPE=postgresql

if ! [ -f /app/data/.initialized ]; then
	  echo "Fresh installation, setting up configuration..."

	  #sed -i "s/%PAPERLESS_CONSUME_REDISHOST%/${CLOUDRON_REDIS_HOST}/g" /app/data/paperless.conf
      SALT=`date +%s|sha256sum|base64|head -c 32`
	  export DOCUMIZESALT=${SALT}
      echo $DOCUMIZESALT > /app/data/salt

	#   sed -i "s/%PAPERLESS_CONSUME_SECRET%/${SECRET}/g" /app/data/paperless.conf
	  touch /app/data/.initialized


fi

RESALT='cat /app/data/salt'
export DOCUMIZESALT=$RESALT
echo $DOCUMIZESALT
echo "=> Ensure permissions"
chown -Rh cloudron:cloudron /app/data
echo "Starting supervisor"
mkdir -p /run/documize && touch /run/documize/supervisord.log
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Documize
