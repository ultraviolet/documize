FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

EXPOSE 5001

RUN mkdir -p /app/code /app/data

WORKDIR /app/code

RUN wget https://documize.s3-eu-west-1.amazonaws.com/downloads/documize-enterprise-linux-amd64 && \
chmod +x documize-enterprise-linux-amd64

COPY start.sh /app/run/
RUN chmod +x /app/run/start.sh

ADD documize-supervisor.conf /etc/supervisor/conf.d/
RUN ln -sf /run/documize/supervisord.log /var/log/supervisor/supervisord.log

CMD [ "/app/run/start.sh" ]